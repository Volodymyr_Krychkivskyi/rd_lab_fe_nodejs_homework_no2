const express = require('express');
const router = express.router();

const {
  getUserById,
  deleteUserById,
  changeUserPasswordById,
} = require('../services/usersService');

const {tryCatchWrapper} = require('../utils/apiUtils');

router.get('/', tryCatchWrapper(async (req, res) => {
  const {userId} = req.user;
  const user = await getUserById(userId);

  res.json({'user': user});
}));

router.delete('/', tryCatchWrapper(async (req, res) => {
  const {userId} = req.user;
  await deleteUserById(userId);

  res.json({'message': 'Success'});
}));

router.patch('/', tryCatchWrapper(async (req, res) => {
  const {userId} = req.user;
  const {oldPassword, newPassword} = req.body;

  await changeUserPasswordById(userId, oldPassword, newPassword);

  res.json({'message': 'Success'});
}));

module.exports = {
  usersRouter: router,
};
