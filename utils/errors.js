/**
 * Represents a NotesError.
 */
class NotesError extends Error {
  /**
  * @param {error.message} message The error.message.
  */
  constructor(message) {
    super(message);
    this.status = 500;
  }
}

/**
 * Represents a InvalidRequestError.
 */
class InvalidRequestError extends NotesError {
  /**
  * @param {error.message} message The error.message.
  */
  constructor(message) {
    super(message);
    this.status = 400;
  }
}

/**
 * Represents a AuthorizationError.
 */
class AuthorizationError extends NotesError {
  /**
  * @param {error.message} message The error.message.
  */
  constructor(message) {
    super(message);
    this.status = 400;
  }
}

/**
 * Represents a DataError.
 */
class DataError extends NotesError {
  /**
  * @param {error.message} message The error.message.
  */
  constructor(message) {
    super(message);
    this.status = 400;
  }
}

module.exports = {
  NotesError,
  InvalidRequestError,
  AuthorizationError,
  DataError,
};
