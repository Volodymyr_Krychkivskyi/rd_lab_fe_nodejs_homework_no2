const express = require('express');
const router = express.router();

const {
  getAllNotesByUserId,
  addNoteToUser,
  getNoteById,
  deleteNoteById,
  checkUncheckNoteById,
  updateNoteById,
} = require('../services/notesService');

const {tryCatchWrapper} = require('../utils/apiUtils');

const {InvalidRequestError} = require('../utils/errors');

router.get('/', tryCatchWrapper(async (req, res) => {
  const {userId} = req.user;
  const {offset, limit} = req.query;
  const notesInfo = await getAllNotesByUserId(userId, offset, limit);
  res.json(notesInfo);
}));

router.post('/', tryCatchWrapper(async (req, res) => {
  const {userId} = req.user;
  const {text} = req.body;
  await addNoteToUser(userId, text);
  res.json({message: 'Success'});
}));

router.get('/:id', tryCatchWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;
  const note = await getNoteById(id, userId);
  if (!note) {
    throw new InvalidRequestError('No note with such id found!');
  }
  res.json({note});
}));

router.delete('/:id', tryCatchWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;
  await deleteNoteById(id, userId);

  res.json({message: 'Success'});
}));

router.patch('/:id', tryCatchWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  await checkUncheckNoteById(id, userId);

  res.json({message: 'Success'});
}));

router.put('/:id', tryCatchWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;
  const {text} = req.body;

  await updateNoteById(id, userId, text);

  res.json({message: 'Success'});
}));

module.exports = {
  notesRouter: router,
};
