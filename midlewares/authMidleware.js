const jwt = require('jsonwebtoken');
const {AuthorizationError} = require('../utils/errors');

require('dotenv').config({path: './.env'});
const SECRET = process.env.SECRET;

const authMiddleware = (req, res, next) => {
  const {authorization} = req.headers;

  if (!authorization) {
    throw new AuthorizationError('Please, provide "authorization" header');
  }

  const [, token] = authorization.split(' ');

  if (!token) {
    throw new AuthorizationError('Please, include token to request');
  }

  try {
    const tokenPayload = jwt.verify(token, SECRET);
    req.user = {
      userId: tokenPayload._id,
      username: tokenPayload.username,
    };
    next();
  } catch (err) {
    throw new AuthorizationError('Wrong token');
  }
};

module.exports = {
  authMiddleware,
};
