const express = require('express');
const router = express.router();

const {
  registration,
  login,
} = require('../services/authService');

const {tryCatchWrapper} = require('../utils/apiUtils');

router.post('/register', tryCatchWrapper(async (req, res) => {
  const {username, password} = req.body;
  await registration({username, password});
  res.json('Success');
}));

router.post('/login', tryCatchWrapper(async (req, res) => {
  const {username, password} = req.body;
  const token = await login({username, password});
  res.json({message: 'Success', token});
}));

module.exports = {
  authRouter: router,
};
