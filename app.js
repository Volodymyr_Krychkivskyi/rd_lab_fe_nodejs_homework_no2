require('dotenv').config();
const PORT = process.env.PORT;
const DB_PATH = process.env.DB_PATH;
// const db = require('db');
// db.connect({
//   PORT: process.env.PORT,
//   // host: process.env.DB_HOST,
//   // username: process.env.DB_USER,
//   // password: process.env.DB_PASS
// });

const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');

const app = express();
// mongoose.connect('mongodb://localhost:27017/test', {useNewUrlParser: true, useUnifiedTopology: true});

const {authMiddleware} = require('./midlewares/authMidleware');
const {authRouter} = require('./controllers/authController');
const {usersRouter} = require('./controllers/usersController');
const {notesRouter} = require('./controllers/notesController');
const {NotesError} = require('./utils/errors');

app.use(express.json());
app.use(cookieParser());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);

app.use(authMiddleware);
app.use('/api/users/me', usersRouter);
app.use('/api/notes', notesRouter);

app.use((err, req, res, next) => {
  if (err instanceof NotesError) {
    console.log('err instanceof NotesError');
    return res.status(err.status).json({message: err.message});
  }
  res.status(500).json({message: err.message});
});

(async () => {
  try {
    await mongoose.connect(DB_PATH, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    });

    // const PORT = process.env.PORT|| 8080;
    app.listen(PORT);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
})();

// module.exports = {
//   SECRET,
//   LIMIT
// }
